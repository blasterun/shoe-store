require 'rails_helper'

RSpec.describe ProductInventoryJob, type: :job do
  include ActiveJob::TestHelper

  let (:store) { create(:store, name: "Nike") }
  let (:product) { create(:product, store: store, name: "Jordan") }

  subject(:job) { described_class.perform_later({store: store.name, model: product.name, quantity: 5}) }

  it 'queues the job' do
    expect { job }
      .to change(enqueued_jobs, :size).by(1)
  end

  it 'creates product inventories' do
    job
    expect { perform_enqueued_jobs }
      .to change(ProductInventory, :count).by(5)
  end
end
