class StoresController < ApplicationController
  def index
    @stores = Store.includes(:products)
  end
end
