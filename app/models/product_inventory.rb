class ProductInventory < ApplicationRecord
  enum status: { on_sale: 0, sold: 1, reserved: 2 }

  belongs_to :product

  # after_create_commit :broadcast_later

  private
    def broadcast_later
      # pr.product.broadcast_render_to pr.product, targer: "products"
      broadcast_replace_later_to product, :product, target: "product"
      # pr.product.broadcast_replace_to pr.product, targer: "products"
      # pr.product.broadcast_replace_later_to pr.product
    end
end
