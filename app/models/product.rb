class Product < ApplicationRecord
  belongs_to :store
  has_many :product_inventories

  def inventory_on_sale_cached
    product_inventories.where(status: [:on_sale]).count
  end
end
