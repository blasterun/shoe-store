class ProductInventoryJob < ApplicationJob
  queue_as :default

  def perform(args)
    return if args[:quantity].zero?

    store = Store.find_by!(name: args[:store])
    product = store.products.find_by!(name: args[:model])

    ProductInventory.insert_all(
      args[:quantity].times.map { |a| { product_id: product.id } }
    )

    product.broadcast_replace_to :products
  end
end
