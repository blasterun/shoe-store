require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  resources :product_inventories
  resources :products
  resources :stores

  root "stores#index"
end
