class CreateProductInventories < ActiveRecord::Migration[7.0]
  def change
    create_table :product_inventories do |t|
      t.integer :status, default: 0
      t.references :product, null: false, foreign_key: true

      t.timestamps
    end
    add_index :product_inventories, :status
  end
end
